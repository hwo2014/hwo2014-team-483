package thesolobot.common;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public enum DataEnum {
//	pI:0 iPD:1.8419904000000002 angle:0.0 currentPieceAngel:0.0 currentThrottle:0.9 currentVelocity:0.7175104000000001 deltaV:0.16903040000000003 gameTick4 ep?:0.18781155555555556
	pieceIndex("pI"), inPieceDistance("iPD"),angle("ang"), pieceAngle("cPA"), currentThrottle("cTh"),
	currentVelocity("cVe"), deltaV("dV"), gameTick("gt"), enginePower("ep?"), deltaAngle("dA"), crashedInPiece("cIP"),
	distanceToAngle("dTA"), lane("lne");

	private String value;
	private DataEnum(String value){
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	@Override
	public String toString(){
		return value;
	}
}
