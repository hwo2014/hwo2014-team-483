package thesolobot.common;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public enum MemoryEnum {
	a("a");
	private String value;
	private MemoryEnum(String value){
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	@Override
	public String toString(){
		return value;
	}
}
