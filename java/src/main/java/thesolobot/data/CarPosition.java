package thesolobot.data;

import java.util.Map;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class CarPosition {
	private Map<String, String> id;
	private double angle;
	private PiecePosition piecePosition;

	public Map<String, String> getId() {
		return id;
	}

	public void setId(Map<String, String> id) {
		this.id = id;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public PiecePosition getPiecePosition() {
		return piecePosition;
	}

	public void setPiecePosition(PiecePosition piecePosition) {
		this.piecePosition = piecePosition;
	}
}
