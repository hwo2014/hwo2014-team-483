package thesolobot.data;

import java.util.Map;

/**
 * Created by samuelpayeur on 4/15/14.
 */

public class Race {
	private Track track;
	private Car[] cars;
	private Map<String, String> raceSession;


	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public Car[] getCars() {
		return cars;
	}

	public void setCars(Car[] cars) {
		this.cars = cars;
	}

	public Map<String, String> getRaceSession() {
		return raceSession;
	}

	public void setRaceSession(Map<String, String> raceSession) {
		this.raceSession = raceSession;
	}
}
