package thesolobot.data;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class PositionDataMessage {
	private String msgType;
	private CarPosition[] data;
	private String gameId;
	private int gameTick;

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public CarPosition[] getData() {
		return data;
	}

	public void setData(CarPosition[] data) {
		this.data = data;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getGameTick() {
		return gameTick;
	}

	public void setGameTick(int gameTick) {
		this.gameTick = gameTick;
	}
}
