package thesolobot.data;

/**
 * Created by samuelpayeur on 4/17/14.
 */
public class Move {
	private double achievedVelocity;
	private double throttle;
	private int pieceIndex;
	private double deltaV;
	private double deltaA;
	private double distanceToAngle;
	private double lane;

	public Move(double desiredVelocity, double approximateThrottle, int pieceIndex, double deltaV, double distanceToAngle, double lane){
		this.achievedVelocity = desiredVelocity;
		this.throttle = approximateThrottle;
		this.pieceIndex = pieceIndex;
		this.deltaV = deltaV;
//		this.deltaA = deltaA;
		this.distanceToAngle = distanceToAngle;
		this.lane = lane;
	}

	public double getDeltaA() {
		return deltaA;
	}

	public void setDeltaA(double deltaA) {
		this.deltaA = deltaA;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}

	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex = pieceIndex;
	}

	public double getDeltaV() {
		return deltaV;
	}

	public void setDeltaV(double deltaV) {
		this.deltaV = deltaV;
	}

	public double getThrottle() {
		return throttle;
	}

	public void setThrottle(double throttle) {
		this.throttle = throttle;
	}

	public double getAchievedVelocity() {
		return achievedVelocity;
	}

	public void setAchievedVelocity(double achievedVelocity) {
		this.achievedVelocity = achievedVelocity;
	}

	public void setDistanceToAngle(double distanceToAngle) {
		this.distanceToAngle = distanceToAngle;
	}

	public double getDistanceToAngle() {
		return distanceToAngle;
	}

	public double getLane() {
		return lane;
	}

	public void setLane(double lane) {
		this.lane = lane;
	}
}
