package thesolobot.data;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public class YourCar {
	private String name;
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
