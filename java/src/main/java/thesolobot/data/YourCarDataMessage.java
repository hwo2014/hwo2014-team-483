package thesolobot.data;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public class YourCarDataMessage {
	private String msgType;
	private YourCar data;

	public YourCar getData() {
		return data;
	}

	public void setData(YourCar data) {
		this.data = data;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
