package thesolobot.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Piece {
	private double length;
	@SerializedName("switch")
	private boolean switchPresent;
	private int radius;
	private double angle;
	private int index;

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public boolean isSwitchPresent() {
		return switchPresent;
	}

	public void setSwitchPresent(boolean switchPresent) {
		this.switchPresent = switchPresent;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
