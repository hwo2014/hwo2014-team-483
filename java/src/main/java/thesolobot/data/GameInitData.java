package thesolobot.data;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class GameInitData {
	private Race race;

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}
}
