package thesolobot.data;

import java.util.Map;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Car {
	public static final String LENGTH = "length";
	public static final String WIDTH = "width";
	public static final String GUIDE = "guideFlagPosition";
	public static final String COLOR = "color";
	public static final String NAME = "name";

	private Map<String, String> id;
	private Map<String, Double> dimensions;

	public Map<String, String> getId() {
		return id;
	}

	public void setId(Map<String, String> id) {
		this.id = id;
	}

	public Map<String, Double> getDimensions() {
		return dimensions;
	}

	public void setDimensions(Map<String, Double> dimensions) {
		this.dimensions = dimensions;
	}
}
