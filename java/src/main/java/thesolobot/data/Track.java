package thesolobot.data;

import java.util.Map;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Track {
	private String id;
	private String name;
	private Lane[] lanes;
	private Piece[] pieces;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Lane[] getLanes() {
		return lanes;
	}

	public void setLanes(Lane[] lanes) {
		this.lanes = lanes;
	}

	public Piece[] getPieces() {
		return pieces;
	}

	public void setPieces(Piece[] pieces) {
		this.pieces = pieces;
	}
}
