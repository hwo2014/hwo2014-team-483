package thesolobot.data;

/**
 * Created by samuelpayeur on 4/17/14.
 */
public class PieceData {
	private double maxThrottle;
	private double minThrottle;
	private double maxAngle;
	private double maxVelocity;
	private double minVelocity;
	private int currentIndex;
	private double pieceAngle;
	private boolean crashedInPiece;

	public PieceData(double maxThrottle, double minThrottle, double maxAngle, double maxVelocity, double minVelocity, int currentIndex, double pieceAngle, boolean crashedInPiece){
		this.maxThrottle = maxThrottle;
		this.minThrottle = minThrottle;
		this.maxAngle = maxAngle;
		this.maxVelocity = maxVelocity;
		this.minVelocity = minVelocity;
		this.currentIndex = currentIndex;
		this.pieceAngle = pieceAngle;
		this.crashedInPiece = crashedInPiece;
	}

	public double getMaxThrottle() {
		return maxThrottle;
	}

	public double getMinThrottle() {
		return minThrottle;
	}

	public double getMaxAngle() {
		return maxAngle;
	}

	public double getMaxVelocity() {
		return maxVelocity;
	}

	public void setMaxVelocity(double maxVelocity){
		this.maxVelocity = maxVelocity;
	}

	public double getMinVelocity() {
		return minVelocity;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public double getPieceAngle() {
		return pieceAngle;
	}

	public boolean hasCrashedInPiece() {
		return crashedInPiece;
	}
}
