package thesolobot.data;

import java.util.Map;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class PiecePosition {
	public static final String ELI = "endLaneIndex";
	public static final String SLI = "startLaneIndex";
	private int pieceIndex;
	private double inPieceDistance;
	private Map<String, Integer> lane;
	private int lap;

	public boolean hasLaneChanged(){
		return lane.get(SLI).equals(lane.get(ELI));
	}

	public int getPieceIndex() {
		return pieceIndex;
	}

	public void setPieceIndex(int pieceIndex) {
		this.pieceIndex = pieceIndex;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public void setInPieceDistance(double inPieceDistance) {
		this.inPieceDistance = inPieceDistance;
	}

	public Map<String, Integer> getLane() {
		return lane;
	}

	public void setLane(Map<String, Integer> lane) {
		this.lane = lane;
	}

	public int getLap() {
		return lap;
	}

	public void setLap(int lap) {
		this.lap = lap;
	}
}
