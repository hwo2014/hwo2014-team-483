package thesolobot.data;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Lane {
	private int distanceFromCenter;
	private int index;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public void setDistanceFromCenter(int distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}
}
