package thesolobot;

import thesolobot.brain.Cortex;
import thesolobot.util.messages.Join;

import java.io.*;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Main {
	public static void main(String... args) throws IOException, ExecutionException, InterruptedException, URISyntaxException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

//		new Main(reader, writer, new Join(botName, botKey));

		new Cortex(reader, writer, botName, new Join(botName, botKey));
	}
//
//	final Gson gson = new Gson();
//	private PrintWriter writer;
//
//	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
//		this.writer = writer;
//		String line = null;
//
//		send(join);
//
//		while((line = reader.readLine()) != null) {
//			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//			if (msgFromServer.msgType.equals("carPositions")) {
//				send(new Throttle(0.5));
//			} else if (msgFromServer.msgType.equals("join")) {
//				System.out.println("Joined");
//			} else if (msgFromServer.msgType.equals("gameInit")) {
//				gson.fromJson(msgFromServer.data.txt.toString(), GameInitData.class);
//				System.out.println("Race init");
//			} else if (msgFromServer.msgType.equals("gameEnd")) {
//				System.out.println("Race end");
//			} else if (msgFromServer.msgType.equals("gameStart")) {
//				System.out.println("Race start");
//			} else {
//				send(new Ping());
//			}
//		}
//	}
//
//	private void send(final SendMsg msg) {
//		writer.println(msg.toJson());
//		writer.flush();
//	}
}
