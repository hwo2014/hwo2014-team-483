package thesolobot.brain.memory;

import thesolobot.common.DataEnum;
import thesolobot.data.Move;
import thesolobot.data.PieceData;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public class DataToMemory {
	private static final String DATAPATH = "/switchData.txt";
	private static final String THMPATH = "src/main/resources/throttleMapping2.csv";
	private static final String MEMPATH = "src/main/resources/memory.csv";
	private static final String ANGPATH = "src/main/resources/angleMapping.csv";

	private final BufferedReader dataReader;

	private FileWriter memoryWriter;
//	private FileWriter throttleWriter;
//	private FileWriter angleWriter;

	private ArrayDeque<Map<String, String>> memoryMap;
	private ArrayDeque<Move> historicalMoves;
	private ArrayDeque<PieceData> pieceDataArrayDeque = new ArrayDeque<>(200);
	private ArrayDeque<Double> recommendedMoves = new ArrayDeque<>(2000);
	private ArrayDeque<PieceData> angleData = new ArrayDeque<>(100);

	private boolean switchSent = false;
	private boolean willSwitch = false;
	private double deltaATolerance = 2.401813180456628;
	private double engineAgility = 0.0204081633;
	private double enginePower = 50;
	private double crashAngle = 60.0;
	private double crashTolerance = 0.9833;
	private PieceData nextAngle;
	private double currentLane = 0;

	public static void main(String[] args) throws IOException {
		new DataToMemory();
	}
	public DataToMemory() throws IOException {
//		throttleWriter = new FileWriter(new File(THMPATH));
		memoryWriter = new FileWriter(new File(MEMPATH));
//		angleWriter = new FileWriter(new File(ANGPATH));

		InputStream resource = this.getClass().getResourceAsStream(DATAPATH);
		dataReader = new BufferedReader(new InputStreamReader(resource));
		memoryMap = new ArrayDeque<>(5000);
		historicalMoves = new ArrayDeque<>(5000);

		processRead();
		analyzeMap();
		analyzeHistoricalMoves();
		addRecommendedMovesToMemory();

		shutdownFileIO();
	}

	private void shutdownFileIO() throws IOException {
		dataReader.close();
//		angleWriter.flush();
//		angleWriter.close();
		memoryWriter.flush();
		memoryWriter.close();
//		throttleWriter.flush();
//		throttleWriter.close();
	}

	private void addRecommendedMovesToMemory() throws IOException {
		Double move = null;
		int gt = 0;
		while((move = recommendedMoves.poll()) != null){
			memoryWriter.write(gt+","+move+"\n");
			memoryWriter.flush();
			gt++;
		}
	}

	private void analyzeHistoricalMoves() {
		ArrayList<Move> pieceMoves = new ArrayList<>();
		incrementNextAngle();
		for(PieceData pieceData : pieceDataArrayDeque){
			int currentIndex = pieceData.getCurrentIndex();
			Move peek = historicalMoves.peek();
			while(peek !=null && peek.getPieceIndex() == currentIndex){
				pieceMoves.add(historicalMoves.poll());
				peek = historicalMoves.peek();
			}
			if(peek!=null) {
				pieceMoves.add(peek);
			}
			ArrayList<Move> moves = analyzePieceMoves(pieceMoves, pieceData);
			for(int i = moves.size()-1; i>=0;--i){
				Move poll = historicalMoves.poll();
				Move move = moves.get(i);
				historicalMoves.addFirst(move);

			}
			pieceMoves.clear();
		}
	}

	private ArrayList<Move> analyzePieceMoves(ArrayList<Move> pieceMoves, PieceData pieceData) {
		ArrayList<Move> updatedMoves = new ArrayList<>();

		for(int i = 0; i<pieceMoves.size()-1; i++){
			if (pieceData.getCurrentIndex()==nextAngle.getCurrentIndex()){
				incrementNextAngle();
			}
			Move currentMove = pieceMoves.get(i);
			Move influencedMove = pieceMoves.get(i+1);
			if(currentLane != currentMove.getLane() &&!willSwitch) {
				recommendedMoves.add(currentMove.getLane());
				willSwitch = true;
				double deltaV = ((currentMove.getThrottle()*10)-currentMove.getAchievedVelocity())/50;
				updateMove(influencedMove, deltaV, currentMove.getAchievedVelocity() + deltaV, currentMove.getThrottle());
				continue;
			}
			else if(currentLane == currentMove.getLane()&&willSwitch){
				willSwitch = false;
			}

			double currentMoveThrottle = currentMove.getThrottle();
			double currentMoveAchievedVelocity = currentMove.getAchievedVelocity();

			if(currentMoveThrottle==1 && (pieceData.getMaxVelocity()>=currentMoveAchievedVelocity)){
				recommendedMoves.add(currentMoveThrottle);
				continue;
			}

			double nextAngleMaxVelocity = nextAngle.getMaxVelocity();
			if(nextAngle.getMaxAngle()/crashAngle<(crashTolerance+0.01)){
				nextAngleMaxVelocity = nextAngleMaxVelocity*1.01;
			}

			double influencedMoveAchievedVelocity = influencedMove.getAchievedVelocity();
			double currentMoveDistanceToAngle = currentMove.getDistanceToAngle();
			double deltaVToNextAngleMax = nextAngleMaxVelocity - currentMoveAchievedVelocity;
			double ticksToAngleAtCurrentVel = Math.ceil(currentMoveDistanceToAngle / currentMoveAchievedVelocity);

			double necessaryThrottle=currentMoveThrottle;
			if(pieceData.getPieceAngle() == 0){
				if(deltaVToNextAngleMax>0){
					double throttleToAngleMax = ((deltaVToNextAngleMax / engineAgility) + nextAngleMaxVelocity)/10;
					if(throttleToAngleMax>1.00){
						double newThrottle = reviseVelocityAndUpdateMove(influencedMove, currentMoveAchievedVelocity, deltaVToNextAngleMax, throttleToAngleMax);
						necessaryThrottle = newThrottle;
					}
					else {
						if(ticksToAngleAtCurrentVel>50){
							double newThrottle = improveVelocityAndUpdateMove(influencedMove, currentMoveAchievedVelocity, deltaVToNextAngleMax+0.5);
							necessaryThrottle = newThrottle;
						}
						else{
							necessaryThrottle = throttleToAngleMax;
							boolean placeholder = true;
						}
					}
				}
				else if(deltaVToNextAngleMax<0.0){
					double deltaVPerTick = deltaVToNextAngleMax / ticksToAngleAtCurrentVel;
					if(currentMoveDistanceToAngle<120){
						necessaryThrottle = regulateVelocityAndUpdateMoves(influencedMove,
								currentMoveAchievedVelocity,
								deltaVPerTick);

					} else {
						double newThrottle = improveVelocityAndUpdateMove(influencedMove, currentMoveAchievedVelocity, Math.abs(deltaVToNextAngleMax)+0.5);
						necessaryThrottle = newThrottle;
					}
				}
			}
			else{
				double angleMaxVel = pieceData.getMaxVelocity();
				if(pieceData.getMaxAngle()/crashAngle<(crashTolerance-0.01)){
					angleMaxVel = pieceData.getMaxVelocity()*1.01;
				}
				double deltaV = angleMaxVel - currentMoveAchievedVelocity;
				double newThrottle = regulateVelocityAndUpdateMoves(influencedMove, currentMoveAchievedVelocity, deltaV);
				if(newThrottle>1){
					newThrottle = reviseVelocityAndUpdateMove(influencedMove, currentMoveAchievedVelocity, deltaV, newThrottle);
				}
				boolean placeHolder = true;
				necessaryThrottle = newThrottle;
//				deltaVPerTick = (currentMoveAchievedVelocity-angleMaxVel) / ticksToAngleAtCurrentVel;
//				necessaryThrottle = regulateVelocityAndUpdateMoves(influencedMove,
//						currentMoveAchievedVelocity,
//						influencedMoveAchievedVelocity,
//						deltaVPerTick);
			}
			if(currentMove.getPieceIndex() != pieceData.getCurrentIndex()){
				updatedMoves.add(currentMove);
			}
			recommendedMoves.add(necessaryThrottle);
		}
		return updatedMoves;
	}

	private double reviseVelocityAndUpdateMove(Move influencedMove, double currentMoveAchievedVelocity, double deltaV, double throttle) {
		double newDeltaV = deltaV / throttle;
		double newVelocity =(currentMoveAchievedVelocity+ newDeltaV);
		double newThrottle = ((newDeltaV/engineAgility)+newVelocity)/10;
		double change = 0.001;
		while(newThrottle>1.00){
			newDeltaV = (deltaV -change);
			newVelocity =(currentMoveAchievedVelocity+ newDeltaV);
			newThrottle = ((newDeltaV/engineAgility)+newVelocity)/10;
			change += 0.001;
		}
		updateMove(influencedMove,newDeltaV, newVelocity, newThrottle);
		return newThrottle;
	}

	private double improveVelocityAndUpdateMove(Move influencedMove, double currentMoveAchievedVelocity, double deltaV) {
		double newDeltaV = deltaV;
		double newVelocity =(currentMoveAchievedVelocity+ newDeltaV);
		double newThrottle = ((newDeltaV/engineAgility)+newVelocity)/10;
		double change = 0.001;
		while(newThrottle<0.991 || newThrottle>1){
			newDeltaV = (deltaV +change);
			newVelocity =(currentMoveAchievedVelocity+ newDeltaV);
			newThrottle = ((newDeltaV/engineAgility)+newVelocity)/10;
			if(newThrottle>1){
				change -= 0.0001;
			}
			else{
				change += 0.0001;
			}
		}
		updateMove(influencedMove, newDeltaV, newVelocity, newThrottle);
		return newThrottle;
	}

	private double regulateVelocityAndUpdateMoves(Move influencedMove, double currentMoveAchievedVelocity, double deltaV) {
		double newVelocity = currentMoveAchievedVelocity + deltaV;
		double newThrottle = ((deltaV/engineAgility) + newVelocity)/10;
		updateMove(influencedMove, deltaV, newVelocity, newThrottle);
		return newThrottle;
	}

	private double newDistanceToAngle(Move influencedMove, double newVelocity) {
		return influencedMove.getDistanceToAngle() - (newVelocity - influencedMove.getAchievedVelocity());
	}

	private void updateMove(Move moveToUpdate, double newDeltaV, double newVelocity, double newThrottle) {
		double distanceToAngle = newDistanceToAngle(moveToUpdate, newVelocity);
		if (distanceToAngle<0){
			int pieceIndex = moveToUpdate.getPieceIndex()+1;
			moveToUpdate.setPieceIndex(pieceIndex);

		}
		moveToUpdate.setDeltaV(newDeltaV);
		moveToUpdate.setDistanceToAngle(distanceToAngle);
		moveToUpdate.setAchievedVelocity(newVelocity);
		moveToUpdate.setThrottle(newThrottle);
	}

	public void processRead() throws IOException {
		String line = null;
		while((line = dataReader.readLine()) != null) {
			Map<String, String> map = new HashMap<>();
			String[] split = line.split(":");
			for(int i = 0; i < split.length; i+=2){
				map.put(split[i].trim(), split[i+1]);
			}
			memoryMap.add(map);
		}
	}

	private void analyzeMap() throws IOException {
		Map<String, String> currentTick = memoryMap.poll();
		Map<String, String> influencedTick = memoryMap.peek();
		currentLane = Double.parseDouble(currentTick.get(DataEnum.lane.getValue()));

		double maxVelocity = 0.0;
		double minVelocity = 1000.0;
		double maxAngle = 0;
		double maxThrottle = 0;
		double minThrottle = 0;
		boolean crashedInPiece = false;

		do {
			double influencedVelocity = Double.parseDouble(influencedTick.get(DataEnum.currentVelocity.getValue()));
			double influencedThrottle = Double.parseDouble(influencedTick.get(DataEnum.currentThrottle.getValue()));
			double currentVelocity = Double.parseDouble(currentTick.get(DataEnum.currentVelocity.getValue()));
			double currentThrottle = Double.parseDouble(currentTick.get(DataEnum.currentThrottle.getValue()));
		//Throttle calculations - for use with different "cars" in order to find engine power;
//			double tvVariance = currentThrottle*10 - currentVelocity;
//			double influencedDeltaV = Double.parseDouble(influencedTick.get(DataEnum.deltaV.getValue()));
//			throttleWriter.write(tvVariance + "," + influencedDeltaV+"\n");

			int upcomingIndex = Integer.parseInt(influencedTick.get(DataEnum.pieceIndex.getValue()));
//			double currentDeltaA = Double.parseDouble(currentTick.get(DataEnum.deltaAngle.getValue()));
//			if(deltaA!=0.0){
//				angleWriter.write(currentIndex+","+currentTick.get(DataEnum.pieceAngle.getValue())+","+
//						currentVel+","+currentTick.get(DataEnum.currentDeltaV.getValue())+","+tvVariance + "," +
//						deltaA+","+currentTick.get(DataEnum.angle.getValue())+"\n");
//
//			}

			int currentIndex = Integer.parseInt(currentTick.get(DataEnum.pieceIndex.getValue()));
			double currentDeltaV = Double.parseDouble(currentTick.get(DataEnum.deltaV.getValue()));
			double pieceAngle = Double.parseDouble(currentTick.get(DataEnum.pieceAngle.getValue()));
			double distanceToAngle = Double.parseDouble(currentTick.get(DataEnum.distanceToAngle.getValue()));
			double lane = Double.parseDouble(currentTick.get(DataEnum.lane.getValue()));
			historicalMoves.add(new Move(currentVelocity, currentThrottle, currentIndex, currentDeltaV, distanceToAngle, lane));
			if(currentIndex != upcomingIndex){
				PieceData pieceData = new PieceData(maxThrottle, minThrottle, maxAngle, maxVelocity, minVelocity, currentIndex, pieceAngle, crashedInPiece);
				pieceDataArrayDeque.add(pieceData);
				if(pieceAngle != 0.0){
					angleData.add(pieceData);
				}
				maxVelocity = 0.0;
				minVelocity = 1000.0;
				maxAngle = 0;
				maxThrottle = 0;
				minThrottle = influencedThrottle;

			}
			else {
				boolean crashed = Boolean.parseBoolean(currentTick.get(DataEnum.crashedInPiece.getValue()));
				if (crashed){
					crashedInPiece = crashed;
				}
				if(influencedVelocity>maxVelocity){
					maxVelocity = influencedVelocity;
				} else if (influencedVelocity<minVelocity){
					minVelocity = influencedVelocity;
				}
				double angle = Math.abs(Double.parseDouble(influencedTick.get(DataEnum.angle.getValue())));
				if(angle>maxAngle){
					maxAngle=angle;
				}
				if(currentThrottle>maxThrottle){
					maxThrottle = currentThrottle;
				} else if (currentThrottle<minThrottle){
					minThrottle = currentThrottle;
				}
			}
			currentTick = influencedTick;
		}while ((influencedTick = memoryMap.poll())!=null);
	}

	private void incrementNextAngle() {
		nextAngle = angleData.poll();
		angleData.add(nextAngle);
	}
}
