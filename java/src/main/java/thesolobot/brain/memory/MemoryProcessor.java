package thesolobot.brain.memory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayDeque;

/**
 * Created by samuelpayeur on 4/16/14.
 */
public class MemoryProcessor {
	private static final String MEMPATH = "/memory.csv";
	private final BufferedReader reader;

	private double[] times;
	private double[] velocities;

	public static void main(String[] args) throws IOException, URISyntaxException {
		new MemoryProcessor();
	}
	public MemoryProcessor() throws IOException, URISyntaxException {
		InputStream resource = this.getClass().getResourceAsStream(MEMPATH);
		reader = new BufferedReader(new InputStreamReader(resource));
	}
	public ArrayDeque<Double> processRead() throws IOException {
		String line = null;
		ArrayDeque<Double> moves = new ArrayDeque<>(2500);
		while((line = reader.readLine()) != null) {
			moves.add(Double.parseDouble(line));
		}
		return moves;
	}
	public void initTimesAndVelocities(int length){
		this.times = new double[length];
		this.velocities = new double[length];
	}
	public void analyzeLap(){

	}

}
