package thesolobot.brain;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import thesolobot.brain.memory.MemoryProcessor;
import thesolobot.data.*;
import thesolobot.util.MsgWrapper;
import thesolobot.util.SendMsg;
import thesolobot.util.messages.Join;
import thesolobot.util.messages.Ping;
import thesolobot.util.messages.SwitchMessage;
import thesolobot.util.messages.Throttle;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Cortex {
	private final MemoryProcessor memoryProcessor;
	private ArrayDeque<Piece> pieces;
	private ArrayDeque<Piece> angles;
	private Piece currentPiece;
	private Piece nextAngle;

	private final ExecutorService executor = Executors.newCachedThreadPool();
	private final Gson gson = new Gson();
	private final PrintWriter writer;
	private final String botName;

	private static final File data = new File("src/main/resources/switchData.txt");
	private final FileWriter fileWriter;

	private double engineAgility = 0.0204081633;
	private double currentThrottle = 1.0;
	private Track track;

	private CarPosition priorPosition = null;
	private double currentVelocity= 0.0;
	private double deltaV;
	private YourCar self;
	private int positionIndex;
	private Lane currentLane = null;
	private Lane[] lanes;
	private boolean crashedInPiece;
	private double deltaA;
	private ArrayDeque<Double> moves;
	private boolean spawned = true;
	private boolean willSwitch = false;
	private boolean switchSent = false;
	private HashMap<Double, Double> velocityLimiters;

	public Cortex(BufferedReader reader, PrintWriter writer, String botName, Join join) throws IOException, ExecutionException, InterruptedException, URISyntaxException {
		this.writer = writer;
		this.botName = botName;
		this.fileWriter = new FileWriter(data, false);
		memoryProcessor = new MemoryProcessor();
//		moves = memoryProcessor.processRead();
		System.out.println("Moves read");
		velocityLimiters = new HashMap<>();
		velocityLimiters.put(-45.0, 6.6137039679642845);
		velocityLimiters.put(45.0, 6.6137039679642845);
		velocityLimiters.put(22.5, 7.555215104973124);
		velocityLimiters.put(-22.5, 7.555215104973124);
		centralProcessing(reader, join);
	}

	private void centralProcessing(BufferedReader reader, Join join) throws IOException, ExecutionException, InterruptedException {
		String line = null;
		send(join);
		simpleRaceInit(reader);

		line = reader.readLine();
		processInitialPositions(gson.fromJson(line, PositionDataMessage.class));

		if (reader.readLine().contains("gameStart")) {
			System.out.println("Race start");
		}

		while((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {
				processCarPositions(gson.fromJson(line, PositionDataMessage.class));

			}
			else if(msgFromServer.msgType.equals("crash")){
				System.out.println(line);
				processCrash();
				this.crashedInPiece = true;
				this.spawned = false;
			}
			else if(msgFromServer.msgType.equals("spawn")){
				System.out.println("Spawned");
				this.spawned = true;
			}
			else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");
			}
			else if(msgFromServer.msgType.equals("lapFinished")){
				System.out.println(line);
			}
			else {
				System.out.println("Unrecognized");
				System.out.println(line);
				send(new Ping());
			}
		}
		System.out.println("End");


		fileWriter.flush();
		fileWriter.close();
		reader.close();
		writer.close();
	}

	private void processCrash() {
		double angle = currentPiece.getAngle();
		velocityLimiters.put(angle, currentVelocity - 0.1);
	}

	private void simpleRaceInit(BufferedReader reader) throws IOException, ExecutionException, InterruptedException {
		String line;
		line = reader.readLine();
		if (gson.fromJson(line, MsgWrapper.class).msgType.equals("join")) {
			System.out.println("Joined");
//			System.out.println(line);

		}
		line = reader.readLine();
//		System.out.println(line);
		YourCarDataMessage yourCarDataMessage = gson.fromJson(line, YourCarDataMessage.class);
		this.self = yourCarDataMessage.getData();

		line = reader.readLine();
		System.out.println(line);
		try{
			GameInitDataMessage gameInitData = gson.fromJson(line, GameInitDataMessage.class);
			processGameInitData(gameInitData.getData());
			memoryProcessor.initTimesAndVelocities(pieces.size());
			System.out.println("Race init");
		} catch (JsonSyntaxException jse){
			System.out.println("Gameline ---");
			System.out.println(line);
			System.out.println(jse.toString());
		}
	}


	private void processInitialPositions(PositionDataMessage positionDataMessage){
		CarPosition[] carPositions = positionDataMessage.getData();
		for(int i = 0; i< carPositions.length; ++i){
			Map<String, String> id = carPositions[i].getId();
			if(self.getName().equals(id.get("name"))&&
					self.getColor().equals(id.get("color"))){
				positionIndex = i;
				priorPosition = carPositions[i];
				Integer laneIndex = carPositions[i].getPiecePosition().getLane().get(PiecePosition.ELI);
				this.currentLane = this.lanes[laneIndex];
				break;
			}
		}
		send(new Throttle(1.0));
	}

	private void processCarPositions(PositionDataMessage positionDataMessage) throws IOException {
		CarPosition pos = positionDataMessage.getData()[positionIndex];
		PiecePosition piecePosition = pos.getPiecePosition();
		if(piecePosition.getPieceIndex()!=currentPiece.getIndex()){
			this.crashedInPiece = false;
			incrementCurrentPiece();
		}
		if (pos.getPiecePosition().hasLaneChanged()){
			Map<String, Integer> lane = pos.getPiecePosition().getLane();
			this.currentLane = this.lanes[lane.get(PiecePosition.ELI)];
		}

		int priorIndex = priorPosition.getPiecePosition().getPieceIndex();

		double velocity = 0.0;
		if(priorIndex!=piecePosition.getPieceIndex()){
			Piece piece = track.getPieces()[priorIndex];

			double length = piece.getLength();
			if(length==0.0){
				int sign =((int)piece.getAngle())>>>31==0? -1 : 1;
				length = Math.abs((piece.getAngle()/360)*(piece.getRadius()+(sign*currentLane.getDistanceFromCenter()))*2*Math.PI);
			}
			velocity = (length +piecePosition.getInPieceDistance()) - priorPosition.getPiecePosition().getInPieceDistance();
		}
		else{
			velocity = piecePosition.getInPieceDistance() - priorPosition.getPiecePosition().getInPieceDistance();
		}
		deltaV = velocity - currentVelocity;
		deltaA = pos.getAngle() - priorPosition.getAngle();
		currentVelocity = velocity;

		int shortestLane = switchLogic();
//		int shortestLane = 0;


		if(willSwitch && !switchSent){
			String direction = null;
			if (lanes[shortestLane].getDistanceFromCenter()>currentLane.getDistanceFromCenter()){
				direction = "Right";
			}
			else if (lanes[shortestLane].getDistanceFromCenter()<currentLane.getDistanceFromCenter()){
				direction = "Left";
			}
			send(new SwitchMessage(direction));
			switchSent = true;
		}
		else{
//			if(Math.abs(deltaA)> 3.0911289190512363){
			currentThrottle = 0.6448;
				send(new Throttle(currentThrottle));
//			}
//			else{
//				calcThrottle(pos);
//			}
		}


		logData(pos, piecePosition, positionDataMessage.getGameTick());
		priorPosition = pos;
	}

	private int switchLogic() {
		int shortestLane = shortestLane();
		if(currentLane.getIndex()!= shortestLane &&!willSwitch) {
			willSwitch = true;
			switchSent = false;
		}
		else if(currentLane.getIndex() == shortestLane&&willSwitch){
			willSwitch = false;

		}
		return shortestLane;
	}

	private int shortestLane(){
		currentLane.getIndex();
		int sign =((int)nextAngle.getAngle())>>>31==0? -1 : 1;
		double currentLaneLength = Math.abs((nextAngle.getAngle()/360)*(nextAngle.getRadius()+(sign*currentLane.getDistanceFromCenter()))*2*Math.PI);
		int shortestLane = currentLane.getIndex();
		for(int i = 0; i< lanes.length;++i){
			if(i==currentLane.getIndex()){
				continue;
			}
			double laneLength = Math.abs((nextAngle.getAngle()/360)*(nextAngle.getRadius()+(sign*lanes[i].getDistanceFromCenter()))*2*Math.PI);
			if(laneLength<currentLaneLength){
				shortestLane = lanes[i].getIndex();
			}
		}
		return shortestLane;
	}

	private void logData(CarPosition pos, PiecePosition piecePosition, int gameTick) throws IOException {
		String str = "pI:" + piecePosition.getPieceIndex() +
				":iPD:" + piecePosition.getInPieceDistance() +
				":ang:" + pos.getAngle() +
				":cPA:" + currentPiece.getAngle() +
				":cTh:" + currentThrottle +
				":cVe:" + currentVelocity +
				":dV:" + deltaV +
				":gt:" + gameTick+
//				":dA:"+ (currentPiece.getAngle()!=0.0?deltaA:0.0)+
				":cIP:"+crashedInPiece+
				":dTA:"+calcDistanceToAngle(piecePosition)+
				":swi:"+(currentLane.getIndex() == shortestLane())+
				":lne:"+currentLane.getDistanceFromCenter()+

				"\n";
		fileWriter.write(str);
		fileWriter.flush();
	}

	private void calcThrottle(CarPosition pos) {
		double distanceToAngle = calcDistanceToAngle(pos.getPiecePosition());
		double angle = nextAngle.getAngle();


		if(angle >=45.0 || angle <=-45.0){
			if(distanceToAngle > 200){
				if(currentVelocity <=6.99&& Math.abs(pos.getAngle())<50){
					currentThrottle = 1.0;
				}
				else{
					currentThrottle = 0.3;
				}
			}
			else{
				if(currentVelocity>6.5951){
					currentThrottle= 0.25;
				}
				else if(currentVelocity<6.5413)
					currentThrottle= 0.7435;
			}
		}
		else if(angle >=22.0 || angle <=-22.0){
			if(distanceToAngle > 200){
				if(currentVelocity <=7.4 && Math.abs(pos.getAngle())<50){
					if (Math.abs(currentPiece.getAngle()) > 40){
						if(currentVelocity>6.5951){
							currentThrottle= 0.25;
						}
					}else {
						currentThrottle = 1.0;
					}
				}
				else{
					currentThrottle -= 0.1;
				}
			}
			else{
				if (Math.abs(currentPiece.getAngle()) > 40){
					if(currentVelocity>6.5951){
						currentThrottle= 0.25;
					}
				}else if(currentVelocity>7.55){
					currentThrottle = 0.45;
				}
				else if(currentVelocity<7.4) {
					currentThrottle = .85;
				}
			}
		}

//		if(spawned) {
//			Double limiter = velocityLimiters.get(currentPiece.getAngle());
//			currentThrottle = moves.poll();
//			if(limiter !=null)
//			{
//				if(currentVelocity>limiter){
//					double necessaryDeltaV = limiter-currentVelocity;
//					double tvVar = necessaryDeltaV / engineAgility;
//					double v = 1- Math.abs((tvVar - limiter)/10);
//					if(v<0){
//						v=0.1;
//					}
//
//					currentThrottle = v;
//					System.out.println("v:"+v+", deltaV:"+necessaryDeltaV+", tvVar:"+tvVar);
//					System.out.println("limiter:"+limiter+", currentVel:"+currentVelocity+", currentThrottle:"+currentThrottle);
//				}
//			}
//			send(new Throttle(currentThrottle));
//		}
//		else{
//			send(new Throttle(1));
//		}
		Throttle throttle = new Throttle(currentThrottle);
		send(throttle);
	}

	private double calcDistanceToAngle(PiecePosition piecePosition){
		double inPieceDistance = piecePosition.getInPieceDistance();
		double length = 0.0;

		int currentPieceIndex = currentPiece.getIndex();
		for(int i = currentPieceIndex; i<nextAngle.getIndex(); i++){
			double pieceLength = track.getPieces()[i].getLength();
			length += pieceLength;
		}
		if(currentPieceIndex > nextAngle.getIndex()){
			for (int i = currentPieceIndex; i < pieces.size() + nextAngle.getIndex(); i++) {
				double pieceLength = track.getPieces()[i%pieces.size()].getLength();
				length += pieceLength;
			}
		}
		if(length==0.0){
			if(currentPiece.getRadius()==0){
				length = currentPiece.getLength();
			}
			else {
				int sign =((int)currentPiece.getAngle())>>>31==0? -1 : 1;
				length = Math.abs((currentPiece.getAngle()/360)*(currentPiece.getRadius()+(sign*currentLane.getDistanceFromCenter()))*2*Math.PI);
			}
		}
		return length-inPieceDistance;
	}

	private void incrementCurrentPiece() {
		currentPiece = pieces.remove();
		pieces.add(currentPiece);
		if(nextAngle.getIndex() == currentPiece.getIndex()){
			incementNextAngle();
		}
	}

	public void processGameInitData(GameInitData data) throws ExecutionException, InterruptedException {
		Race race = data.getRace();
		this.track = race.getTrack();
		lanes = track.getLanes();
		pieceInit(track.getPieces());
	}

	private void pieceInit(Piece[] pieces) throws ExecutionException, InterruptedException {
		ArrayDeque<Piece> angles = new ArrayDeque<Piece>(pieces.length);
		ArrayDeque<Piece> segments = new ArrayDeque<Piece>(pieces.length);

		for(int i = 0; i < pieces.length; ++i){
			Piece piece = pieces[i];
			piece.setIndex(i);
			segments.add(piece);
			if(piece.getAngle()!=0){
				angles.add(piece);
			}
		}
		setAngles(angles);
		setPieces(segments);

		incementNextAngle();
		incrementCurrentPiece();
	}

	private void incementNextAngle() {
		nextAngle = angles.remove();
		angles.add(nextAngle);
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

	public void setAngles(ArrayDeque<Piece> angles) {
		this.angles = angles;
	}

	public void setPieces(ArrayDeque<Piece> pieces){
		this.pieces = pieces;
	}
}
