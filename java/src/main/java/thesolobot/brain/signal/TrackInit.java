package thesolobot.brain.signal;

import thesolobot.brain.Cortex;
import thesolobot.data.Piece;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class TrackInit implements Runnable{//<ArrayDeque<Piece>> {

	private List<Piece> pieces;
	private Cortex cortex;
	public TrackInit(List<Piece> pieces, Cortex cortex){
		this.pieces = pieces;
		this.cortex = cortex;
	}
	@Override
	public void run() {
		ArrayDeque<Piece> angles = new ArrayDeque<Piece>(pieces.size());
		ArrayDeque<Piece> segments = new ArrayDeque<Piece>(pieces.size());

		for(int i = 0; i < pieces.size(); ++i){
			Piece piece = pieces.get(i);
			piece.setIndex(i);
			segments.add(piece);
			if(piece.getAngle()!=0){
				angles.add(piece);
			}
		}
		cortex.setAngles(angles);
		cortex.setPieces(segments);
	}
}
