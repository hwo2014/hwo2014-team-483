package thesolobot.util;

import com.google.gson.Gson;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}