package thesolobot.util.messages;

import thesolobot.util.SendMsg;

/**
 * Created by samuelpayeur on 4/18/14.
 */
public class SwitchMessage extends SendMsg {
	private Object data;

	public SwitchMessage(String data){
		this.data = data;
	}
	@Override
	protected String msgType() {
		return "switchLane";
	}

	@Override
	protected Object msgData(){
		return data;
	}
}
