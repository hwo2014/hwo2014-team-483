package thesolobot.util.messages;

import thesolobot.util.SendMsg;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}
