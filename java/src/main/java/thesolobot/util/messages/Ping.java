package thesolobot.util.messages;

import thesolobot.util.SendMsg;

/**
 * Created by samuelpayeur on 4/15/14.
 */
public class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}
